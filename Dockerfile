FROM registry.access.redhat.com/fuse7/fuse-java-openshift:1.4
ARG gracefultime=13
ARG PORT=8080
ARG APP_NAME
ENV GRACEFUL_TIME=${gracefultime}
ENV JAVA_APP_DIR=/deployments
EXPOSE ${PORT} 8778 9779
COPY target/${APP_NAME} /deployments/
