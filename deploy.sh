#!/usr/bin/env bash

keycloakurl="192.168.1.110"
zipkinurl="192.168.1.110"
teamurl="192.168.1.110"
echo "Basic Compiling with sopra profile"


usage="
$(basename "$0")
Current Options: 
[--help|-h] 
[--keycloak-ip]
[--zipkin-ip]
[--team-ip]

------------------------------
    --help|-h visualize this help text box
    --keycloak-ip overwrites ip to reach keycloak container host
    --zipkin-ip overwrites ip to reach zipkin container host
    --team-ip overwrites ip to reach soccer team container host
------------------------------
"
process_args () {
options=$(getopt --options  -h -l keycloak-ip:,zipkin-ip:,team-ip:,help -- "$@")

[ $? -eq 0 ] || {

        echo "Incorrect option provided"

        echo "$usage"

        exit 1

    }

eval set -- "$options"

    while [ $# -gt 0 ]; do

     case "$1" in
     --keycloak-ip)shift; keycloakurl=($1) ;;
     --zipkin-ip)shift; zipkinurl=($1) ;;
     --team-ip)shift; teamurl=($1) ;;
     -h |--help) echo "$usage"
      exit
     ;;
      --)
           shift

            break

            ;;

        esac

        shift

    done

}



process_args "$@"




#mvn clean package -Psopra -DZIPKIN_URL=http://$zipkinurl:9411 -DKEYCLOAK_URL=http://$keycloakurl:8081/auth -DKEYCLOAK_REALM=sopra-ing -DKEYCLOAK_CI=cli-sopra-ing -DDB_USER=sopra -DDB_PASSWORD=sopra123 -DGRACEFUL_TIME=10 -s assets/settings.xml


echo "Adding firewall Rules"
sudo firewall-cmd --zone=public --add-port=8080/tcp  --add-port=9411/tcp  --add-port=8081/tcp --permanent
sudo firewall-cmd --reload

echo "Copiying sopra realm to tmp folder"
sudo cp assets/sopra-ing.json /tmp/sopra-ing.json

sudo docker run -d --name keycloak -p 8081:8080  -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT="/tmp/sopra-ing.json -Dkeycloak.profile.feature.upload_scripts=enabled" -v /tmp/sopra-ing.json:/tmp/sopra-ing.json  jboss/keycloak
echo ""
echo ""
echo "#===========================================================================================================================================#"
echo "#																		  #"
echo "After keycloak container will be running you will be able to manage via web on http://$keycloakurl:8081/auth with credentials admin/admin   #"
echo "#																		  #"
echo "#===========================================================================================================================================#"
echo ""
echo ""

echo "Waiting for keycloak starting up (1 min aprox)"
sleep 60
echo "Deploying zipkin system traceability"
docker run -d --name zipkin -p 9411:9411 openzipkin/zipkin

echo ""
echo ""
echo "#===================================================================================================#"
echo "#													  #"
echo "#After zipkin container will be running you will be able to manage via web on http://$zipkinurl:9441#"
echo "#													  #"
echo "#===================================================================================================#"
echo ""
echo ""


echo "Deploying soccer team container"
docker run -d --name team  -e ZIPKIN_URL=http://$zipkinurl:9411 -e KEYCLOAK_URL=http://$keycloakurl:8081/auth -e KEYCLOAK_REALM=sopra-ing -e KEYCLOAK_CI=cli-sopra-ing -e DB_USER=sopraadm -e DB_PASSWORD=sopra654 -e GRACEFUL_TIME=8    -p 8080:8080  charlie335/sopra-team:0.0.2

echo ""
echo ""
echo ""
echo "#=============================================================================================================================================================================================================#"
echo "#																									#"
echo "# After team container will be able to perform, you can access to h2 gui via http://$teamurl:8080/ING/h2-ui/ with credentials sopraadm/sopra654 and jdbc:h2:mem:testdb driver and swagger via http://$teamurl:8080/ING/api/swagger-ui.html#"
echo "#																									  #"
echo "#=============================================================================================================================================================================================================#"
echo ""
echo ""
echo ""
echo ""


docker logs -f team
