package co.com.sopra;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.fasterxml.jackson.databind.ObjectWriter;
import co.com.sopra.model.dto.request.TeamCreationRequest;
import lombok.extern.slf4j.Slf4j;

@ActiveProfiles("test")
@Slf4j
@SpringBootTest(classes = SoccerTeamApplication.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class SoccerTeamControllerTest {

	@Autowired
	private MockMvc mvc;

	@Test
	void createTeam400Status() throws Exception {
		mvc.perform(put("/create/team")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isBadRequest());

	}

	@Test
	void createTeam500Status() throws Exception {
		TeamCreationRequest request = setCreateRequest("Barcelona", "141 846 socios", null);
		request.setCity(null);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(put("/create/team")
				.contentType(MediaType.APPLICATION_JSON).header("X-API-VERSION", "v1")
				.content(requestJson))
				.andExpect(status()
				.isInternalServerError())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errorMsg", is("could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement")));

		
	}
	
	@Test
	void createTeamAndList201Status() throws Exception {
		TeamCreationRequest request = setCreateRequest("Barcelona", "141 846 socios", null);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(put("/create/team")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1")
				.content(requestJson))
				.andExpect(status().isCreated())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id", is(2)));

		request = setCreateRequest("Real Valladolid", "Ronaldo", 800000L);
		requestJson = ow.writeValueAsString(request);
		mvc.perform(put("/create/team").
				contentType(MediaType.APPLICATION_JSON).
				header("X-API-VERSION", "v1")
				.content(requestJson))
				.andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id", is(3)));
		
		
		mvc.perform(get("/list/team/all")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isFound())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.teams[0].id", is(3)))
				.andExpect(jsonPath("$.teams[1].id", is(2)));
				
	}
	
	@Test
	void ListTeams302Status() throws Exception {
		mvc.perform(get("/list/team/all")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isNotFound());
	}
	
	@Test
	void updateFail() throws Exception {
		TeamCreationRequest request = setCreateRequest("Barcelona", "141 846 socios", null);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);
		mvc.perform(post("/update/team/100").
				contentType(MediaType.APPLICATION_JSON).
				header("X-API-VERSION", "v1")
				.content(requestJson))
				.andExpect(status().isNotFound());
	}
	
	@Test
	void updateSuccess() throws Exception {
		TeamCreationRequest request = setCreateRequest("Real Madrid", "101 115 socios", 100000L);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);
		mvc.perform(post("/update/team/3").
				contentType(MediaType.APPLICATION_JSON).
				header("X-API-VERSION", "v1")
				.content(requestJson))
				.andExpect(status().isOk());
		
		mvc.perform(get("/list/team/all")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isFound())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.teams[0].id", is(2)))
				.andExpect(jsonPath("$.teams[1].id", is(3)));
	}
	
	@Test 
	void getTeamDetailFail() throws Exception{
		mvc.perform(get("/get/team/9999/details")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isNotFound())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errorMsg", is("No Data found by id 9999")));
	}
	
	@Test 
	void getTeamDetailSuccess() throws Exception{
		mvc.perform(get("/get/team/2/details")
				.contentType(MediaType.APPLICATION_JSON)
				.header("X-API-VERSION", "v1"))
				.andExpect(status().isFound())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id", is(2)));
	}

	
	
	
	
	

	TeamCreationRequest setCreateRequest(String city, String owner, Long capacity) {
		long spectReq = 300000L;
		long spectLimit = 500000L;
		long spectatorsCapacity = spectReq + (long) (Math.random() * (spectLimit - spectReq));
		int players = 30 + (int) (Math.random() * (100 - 30));
		TeamCreationRequest request = new TeamCreationRequest();
		request.setCity(city);
		request.setCompetition("La Liga");
		request.setName(city.concat(" FC"));
		request.setOwner(owner);
		request.setPlayersQuantity(players);
		request.setSpectatorsCapacity(null != capacity ? capacity : spectatorsCapacity);

		return request;
	}

}
