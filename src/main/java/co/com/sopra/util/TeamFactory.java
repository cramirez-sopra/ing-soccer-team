package co.com.sopra.util;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import co.com.sopra.model.dto.abst.TeamAbstract;
import co.com.sopra.model.dto.response.TeamResponse;
import co.com.sopra.model.entity.TeamEntity;

@Component
public class TeamFactory {

	/**
	 * Convert Json Request to Standard Team entity
	 * 
	 * @param request
	 * @return Json Team Request as Database Entity
	 */
	public TeamEntity populateEntity(TeamAbstract request) {
		TeamEntity entity = TeamEntity.builder().city(request.getCity()).competition(request.getCompetition())
				.name(request.getName()).owner(request.getOwner()).playersQuantity(request.getPlayersQuantity())
				.spectatorsCapacity(request.getSpectatorsCapacity()).build();
		return entity;
	}

	/**
	 * Convert Database Entity to a Json Response
	 * 
	 * @param entity
	 * @param abstResponse
	 */
	public void convertEntityToResponse(final TeamEntity entity, TeamAbstract abstResponse, boolean isFullDetails) {
		if (null != abstResponse) {
			abstResponse.setName(entity.getName());
			abstResponse.setId(entity.getId());
			abstResponse.setSpectatorsCapacity(entity.getSpectatorsCapacity());

			if (isFullDetails) {
				abstResponse.setCity(entity.getCity());
				abstResponse.setCompetition(entity.getCompetition());
				abstResponse.setOwner(entity.getOwner());
				abstResponse.setPlayersQuantity(entity.getPlayersQuantity());
			}
		}

	}

	/**
	 * convert entity list to json response list
	 * 
	 * @param <TeamEntity>
	 * @param <TeamResponse>
	 * @param from
	 * @param func
	 * @return
	 */
	@SuppressWarnings("hiding")
	public static <TeamEntity, TeamResponse> List<TeamResponse> convertList(List<TeamEntity> from,
			Function<TeamEntity, TeamResponse> func) {
		return from.stream().map(func).collect(Collectors.toList());
	}

	/**
	 * populate Team Response with url Detail
	 * 
	 * @param entity
	 * @return
	 */
	public TeamResponse convertEntityToJsonResponse(TeamEntity entity, boolean isFullDetails) {
		TeamResponse response = new TeamResponse();
		convertEntityToResponse(entity, response, isFullDetails);
		if (!isFullDetails) {
			response.setDetailsUrl("/get/team/".concat(String.valueOf(response.getId()).concat("/details")));
		}

		return response;
	}

}
