package co.com.sopra.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.sopra.model.dto.request.TeamCreationRequest;
import co.com.sopra.model.dto.response.TeamListAllResponse;
import co.com.sopra.model.dto.response.TeamResponse;
import co.com.sopra.svc.SoccerTeamService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TeamController {

	private final SoccerTeamService soccerSvc;

	public TeamController(SoccerTeamService soccerSvc) {
		super();
		this.soccerSvc = soccerSvc;
	}

	@PutMapping(value = "${spring.application.services.api.insert}", headers = { "X-API-VERSION=v1" })
	public ResponseEntity<TeamResponse> insertTeam(@RequestBody TeamCreationRequest request) {
		log.debug("Insert Method Invoked");
		return soccerSvc.insertTeam(request);
	}

	@GetMapping(value = "${spring.application.services.api.listAll}", headers = { "X-API-VERSION=v1" })
	public ResponseEntity<TeamListAllResponse> ListAllTeams() {
		log.debug("List All Method Invoked");
		return soccerSvc.listAllTeams();
	}

	@GetMapping(value = "${spring.application.services.api.listDetail}", headers = { "X-API-VERSION=v1" })
	public ResponseEntity<TeamResponse> getTeamDetails(@PathVariable long id) {
		log.debug("Get Detail Method Invoked");
		return soccerSvc.getTeamDetails(id);
	}

	@PostMapping(value = "${spring.application.services.api.update}", headers = { "X-API-VERSION=v1" })
	public ResponseEntity<TeamResponse> updateTeam(@PathVariable long id, @RequestBody TeamCreationRequest request) {
		log.debug("Insert Method Invoked");
		return soccerSvc.updateTeam(id,request);
	}

}
