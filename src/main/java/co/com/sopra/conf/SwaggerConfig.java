package co.com.sopra.conf;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfig implements WebMvcConfigurer {

	
    @Value("${rhsso.role-authorization}")
    private String roleApp;
	  @Bean
	    public Docket api() { 
	        return new Docket(DocumentationType.SWAGGER_2)  
	          .select()                                  
	          .apis(RequestHandlerSelectors.basePackage("co.com.sopra.controller"))    
	          .paths(PathSelectors.any())                          
	          .build() 
	        .securitySchemes(Arrays.asList(apiKey()))
            .securityContexts(Collections.singletonList(securityContext()));
	    }
	  
	    private ApiKey apiKey() {
	        return new ApiKey("Bearer", "Authorization", "header");
	    }
	    private SecurityContext securityContext() {
	        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*")).build();
	    }

	    private List<SecurityReference> defaultAuth() {
	        final AuthorizationScope authorizationScope = new AuthorizationScope(this.roleApp, "Credenciais do APP (TERMINAL)");
	        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[]{authorizationScope};
	        return Collections.singletonList(new SecurityReference("Bearer", authorizationScopes));
	    }
	  
	  @Override
	  public void addViewControllers(ViewControllerRegistry registry) {
	      registry.addRedirectViewController("/api/v2/api-docs", "/v2/api-docs");
	      registry.addRedirectViewController("/api/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
	      registry.addRedirectViewController("/api/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
	      registry.addRedirectViewController("/api/swagger-resources", "/swagger-resources");
	  }

	  @Override
	  public void addResourceHandlers(ResourceHandlerRegistry registry) {
	      registry.addResourceHandler("/api/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
	      registry.addResourceHandler("/api/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	  }
}
