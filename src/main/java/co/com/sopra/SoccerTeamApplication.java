package co.com.sopra;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.sbb.esta.openshift.gracefullshutdown.GracefulshutdownSpringApplication;

@SpringBootApplication
public class SoccerTeamApplication {

	public static void main(String[] args) {
		GracefulshutdownSpringApplication.run(SoccerTeamApplication.class, args);
	}

}
