package co.com.sopra.svc;

import org.springframework.http.ResponseEntity;

import co.com.sopra.model.dto.request.TeamCreationRequest;
import co.com.sopra.model.dto.response.TeamResponse;
import co.com.sopra.model.dto.response.TeamListAllResponse;

public interface SoccerTeamService {
	/**
	 * Create a soccer team
	 * 
	 * @param request
	 * @return soccer team created with succes 201 http code if it was able to be
	 *         created
	 */
	public ResponseEntity<TeamResponse> insertTeam(TeamCreationRequest request);

	/**
	 * Find all soccer teams sorted by spectators capacity desc
	 * 
	 * @return a list of soccer teams if there are exists with 302 http status, if
	 *         the array is empty, it'll return 404 not found
	 */
	public ResponseEntity<TeamListAllResponse> listAllTeams();

	/**
	 * Find a detailed specification of a soccer team
	 * 
	 * @param id
	 * @return a Soccer Team response if the id exists on database, if it's success
	 *         the http returned code will be 302, if not it will be 404
	 */
	public ResponseEntity<TeamResponse> getTeamDetails(long id);
	
	
	/**
	 * Update a current object if there is exists on database
	 * @param request
	 * @return
	 */
	public ResponseEntity<TeamResponse> updateTeam(long id,TeamCreationRequest request);
}
