package co.com.sopra.svc.impl;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.com.sopra.model.dto.request.TeamCreationRequest;
import co.com.sopra.model.dto.response.TeamResponse;
import co.com.sopra.model.dto.response.TeamListAllResponse;
import co.com.sopra.model.entity.TeamEntity;
import co.com.sopra.model.repository.TeamRepository;
import co.com.sopra.svc.SoccerTeamService;
import co.com.sopra.util.TeamFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@Service
public class SoccerTeamServiceImpl implements SoccerTeamService {

	private final TeamRepository repository;
	private final TeamFactory factory;
	private HttpStatus status;

	public SoccerTeamServiceImpl(TeamRepository repository, TeamFactory factory) {
		super();
		this.repository = repository;
		this.factory = factory;
	}

	/**
	 * insert team on database
	 */
	@Override
	public ResponseEntity<TeamResponse> insertTeam(TeamCreationRequest request) {
		log.info("populating entity");
		try {
			TeamEntity entity = factory.populateEntity(request);
			entity.setCreationDate(new Date());

			repository.save(entity);
			TeamResponse response = new TeamResponse();
			factory.convertEntityToResponse(entity, response, true);
			response.setCreationDate(entity.getCreationDate());

			log.info("Team Created @Database= {}", response.toString());

			return new ResponseEntity<TeamResponse>(response, HttpStatus.CREATED);
		} catch (Exception e) {
			log.error("An error has been ocurred, {}", e.getMessage());
			TeamResponse response = TeamResponse.builder().errorMsg(e.getMessage()).build();
			return new ResponseEntity<TeamResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * get whole list of soccert team is the list isn't empty
	 */
	@Override
	public ResponseEntity<TeamListAllResponse> listAllTeams() {

		log.info("reach out all database registries");
		HttpStatus status = HttpStatus.FOUND;
		try {
			log.info("looking for whole team crews");
			List<TeamEntity> entityResponse = repository.findAll();
			List<TeamResponse> teamList = TeamFactory
					.convertList(entityResponse, entity -> factory.convertEntityToJsonResponse(entity, false)).stream()
					.sorted(Comparator.comparingLong(TeamResponse::getSpectatorsCapacity).reversed())
					.collect(Collectors.toList());

			if (teamList.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				throw new Exception("No Data was found ");
			}

			TeamListAllResponse response = TeamListAllResponse.builder().teams(teamList).build();
			log.info("teams list obtained successfuly");
			return new ResponseEntity<TeamListAllResponse>(response, status);
		} catch (Exception e) {
			log.error("{}", e.getMessage());
			status = status == HttpStatus.FOUND ? HttpStatus.INTERNAL_SERVER_ERROR : status;
			TeamListAllResponse response = TeamListAllResponse.builder().errorMsg(e.getMessage()).build();
			return new ResponseEntity<TeamListAllResponse>(response, status);
		}

	}

	/**
	 * looking for a specific team
	 */
	@Override
	public ResponseEntity<TeamResponse> getTeamDetails(long id) {
		log.info("team id {} needs to be reach", id);
		status = HttpStatus.FOUND;
		try {
			log.info("looking for team id {} to find details", id);
			TeamResponse response = new TeamResponse();
			factory.convertEntityToResponse(findTeamById(id), response, true);
			log.info("team id {} details obtained successfuly", id);
			return new ResponseEntity<TeamResponse>(response, status);
		} catch (Exception e) {
			log.error("{}", e.getMessage());
			status = status == HttpStatus.FOUND ? HttpStatus.INTERNAL_SERVER_ERROR : status;
			TeamResponse response = TeamResponse.builder().errorMsg(e.getMessage()).build();
			return new ResponseEntity<TeamResponse>(response, status);
		}

	}

	/**
	 * update a team if there is exist
	 */
	@Override
	public ResponseEntity<TeamResponse> updateTeam(long id,TeamCreationRequest request) {
		status = HttpStatus.OK;
		try {
			log.info("looking for team id {} to update", id);
			TeamEntity entity = findTeamById(id);
			
			TeamEntity entityUpdated = factory.populateEntity(request);
			entityUpdated.setId(entity.getId());
			entityUpdated.setCreationDate(entity.getCreationDate());
			repository.save(entityUpdated);

			TeamResponse response = new TeamResponse();
			factory.convertEntityToResponse(entityUpdated, response, true);
			log.info("team id {} updated successfuly", id);
			return new ResponseEntity<TeamResponse>(response, status);

		} catch (Exception e) {
			log.error("{}", e.getMessage());
			status = status == HttpStatus.OK ? HttpStatus.INTERNAL_SERVER_ERROR : status;
			TeamResponse response = TeamResponse.builder().errorMsg(e.getMessage()).build();
			return new ResponseEntity<TeamResponse>(response, status);
		}

	}

	/**
	 * find by team id
	 * 
	 * @param id
	 * @return an entity extracted by id if there is exist that team id
	 * @throws Exception
	 */
	private TeamEntity findTeamById(long id) throws Exception {
		TeamEntity entity = repository.findById(id).orElse(null);

		if (null == entity) {
			status = HttpStatus.NOT_FOUND;
			throw new Exception("No Data found by id ".concat(String.valueOf(id)));
		}

		return entity;
	}

}
