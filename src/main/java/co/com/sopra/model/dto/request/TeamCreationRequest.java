package co.com.sopra.model.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import co.com.sopra.model.dto.abst.TeamAbstract;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@JsonIgnoreProperties
@NoArgsConstructor
public class TeamCreationRequest extends TeamAbstract implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 8282404235828124577L;


	
}
