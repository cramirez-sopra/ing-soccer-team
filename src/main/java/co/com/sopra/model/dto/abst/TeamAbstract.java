package co.com.sopra.model.dto.abst;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TeamAbstract {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("city")
	private String city;
	
	@JsonProperty("owner")
	private String owner;
	
	@JsonProperty("spectatorsCapacity")
	private Long spectatorsCapacity;
	
	@JsonProperty("competition")
	private String competition;
	
	@JsonProperty("playersQuantity")
	private Integer playersQuantity;
	

}
