package co.com.sopra.model.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Builder
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class TeamListAllResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1876409278787060187L;

	private List<TeamResponse> teams;

	@JsonProperty("errorMsg")
	private String errorMsg;

	public List<TeamResponse> getTeams() {
		if (null == this.teams) {
			teams = new ArrayList<>();
		}
		return this.teams;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

}
