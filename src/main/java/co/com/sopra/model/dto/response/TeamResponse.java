package co.com.sopra.model.dto.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import co.com.sopra.model.dto.abst.TeamAbstract;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@JsonIgnoreProperties
@JsonInclude(value = Include.NON_NULL)
public class TeamResponse extends TeamAbstract implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5057458685795247276L;

	@JsonProperty("creationDate")
	private Date creationDate;
	
	@JsonProperty("detailsUrl")
	private String detailsUrl;
	
	@JsonProperty("errorMsg")
	private String errorMsg;
}

