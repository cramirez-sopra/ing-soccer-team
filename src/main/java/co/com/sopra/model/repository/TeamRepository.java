package co.com.sopra.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.sopra.model.entity.TeamEntity;


public interface TeamRepository extends JpaRepository<TeamEntity, Long>{

}
