package co.com.sopra.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "team")
@ToString
public class TeamEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "name", length = 200, nullable = false)
	private String name;

	@Column(name = "city", length = 200, nullable = false)
	private String city;
	
	@Column(name = "owner", length = 200)
	private String owner;
	
	@Column(name = "spectatorscapacity", nullable = false)
	private long spectatorsCapacity;
	
	@Column(name = "competition", length = 200)
	private String competition;
	
	@Column(name = "playersquantity", nullable = false)
	private int playersQuantity;
	
	@Column(name = "creationdate" )
	private Date creationDate;
	
	
}
