# ing-soccer-team

- El proyecto está empaquetado con Maven, y con Java8 como runtime

- Dentro de los assets encontramos los siguientes archivos:
- [ ] ./assets/Dockerfile | para la creación del contenedor docker
- [ ] ./assets/settings.xml |  para compilar con el empaquetador de maven
- [ ] ./assets/sopra-ing.json |   para cargar por defecto el realm de keycloak dentro del contenedor
- [ ] ./assets/Sopra Steria - ING.postman_collection.json  |  colección de ejemplo para probar el proyecto y obtener token de keycloak
- [ ] ./deploy.sh   |   nos ayuda a desplegar de manera autónoma los contenedores dentro de la misma máquina host, e.g comando:
`./deploy.sh --keycloak-ip 192.168.1.110 --zipkin-ip 192.168.1.110 --team-ip 192.168.1.110 `

para más ayudas del script de despliegue ejecutar:
`./deploy.sh --help`
se requiere que el usuario root ejecute el script ya que el contenedor de keycloak require compartir volumen en la máquina hospedadora por el cargue del reino por defecto.

- Al ejecutar el script de despliegue se hará uso de ayudas textuales para los ingresos a los servicios de zipkin, keycloak y de swagger y la interfaz de h2.

- el script está hecho con propósito desarrollo por lo tanto no está optimizado 100% dinámico, sólo lo necesario como ajustes de ips.

- La solución contempla las siguientes apis:

- [ ]  Crear Equipos
- [ ]  Listar Equipos
- [ ]  Obtener detalles de un equipo 
- [ ]  Actualizar equipo

El proyecto se trabajó con las siguientes librerías mvn:

- - spring-boot-dependencies
- - spring-cloud-dependencies
- - lombok |para organizar mejor los atributos de clases o entidades
- - spring-cloud-context
- - spring-boot-starter-actuator  |  para métricas y checkeo del contenedor, requisito para la librería de gracefultime
- - springboot-graceful-shutdown  |  para administrar el tiempo de gracia del aplicatión al momento de recibir la señal de kill
- - org.slf4j |para manejo de logs
- - spring-boot-starter-data-jpa |  para manejo de bases de datos
- - h2 |como motor de bd en memoria
- - springfox-swagger2 | para activar la documentación de los métodos
- - spring-cloud-starter-zipkin |  para la trazabilidad distribuida
- - spring-cloud-starter-sleuth |  para la trazabilidad distribuida
- - spring-boot-starter-test |  para pruebas unitarias
- - junit-jupiter-engine |  para pruebas unitarias
- - spring-boot-starter-security |  para manejo de seguridad
- - keycloak-spring-boot-2-starter |  para manejo de seguridad



El proyecto cuenta con tres perfiles maven:
- [ ] dev |  para pruebas de desarrollo
- [ ] sopra |  para creación del aplicativo con parámetros dinámicos
- [ ] test |  para las pruebas unitarias

- para ejecutar el perfil dev basta con ejecutar el siguiente comando, estando sobre la ruta del proyecto maven:
`mvn clean package -Pdev -s assets/settings.xml`

- para ejecutar el perfil sopra basta con ejecutar el siguiente comando, estando sobre la ruta del proyecto maven:
`mvn clean package -Psopra -DZIPKIN_URL=http://192.168.1.110:9411 -DKEYCLOAK_URL=http://192.168.1.110:8081/auth -DKEYCLOAK_REALM=sopra-ing -DKEYCLOAK_CI=cli-sopra-ing -DDB_USER=sopra -DDB_PASSWORD=sopra123 -DGRACEFUL_TIME=10 -s assets/settings.xml`

- para construir imagen docker podemos ejecutar el siguiente comando estando en el root dentro del proyecto maven (reemplazar tag al gusto deseado):
`docker build -t charlie335/sopra-team:0.0.2  --build-arg gracefultime=5 --build-arg PORT=8080 --build-arg APP_NAME=soccer-team-0.0.2.jar .`

la máquina en donde se disponga a ejecutar o debe tener el firewall deshabilitado o con permisos en los puertos tcp 8080, 8081 y 9411 (el script realiza esta operación por medio del cmd firewall-cmd).



# Mejoras que realizaría para multiambiente - productivo

- [ ] Configuración profile de mvn kubernetes, con el fin de generar de manera dinámica los recursos requeridos (cm,secrets,sa,svc,ingress...)
- [ ] framework webflux, para manejo de recursos mediante el patrón reactivo
- [ ] manejo de pruebas sast por medio de sonarQube u otro tipo de analizador estático de código
- [ ] Manejo de otro engine de base de datos
- [ ] Mejora en el diseño de las entidades de la db
- [ ] Manejo de plugin de flyway debe para manejo de script automáticos de bd
- [ ] Manejo del plugin de fabric8 para generar recursos de kubernetes
- [ ] Manejo de logs mediante el stack de EFK
- [ ] administración de apis mediante un api manager como 3scale o apiGee
- [ ] Uso de un ALM para el manejo de las HU
- [ ] Ciclo de vida devios con CI/CD, con herramientas como Jenkins y Ansible, en vez del uso de script de despliegue por bash
- [ ] Externalización de recursos de la aplicación tales como el logback y el application.yaml a recursos kubernetes
- [ ] Manejo de variables de entorno por medio de CM o información sensible por medio de Secrets
- [ ] Uso del protocolo seguro por medio de TLS
- [ ] Validación de los parámetros para la inserción en base de datos.

